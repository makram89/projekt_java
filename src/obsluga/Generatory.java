package obsluga;

import byty_ogladane.*;


import java.util.*;

/**
 * Klasa zawiera generatory do tworzenia przypadkowych pozycji w symulacji
 */
public class Generatory {

    private Random rand = new Random();

    public Dystrybutor generator_dystrybutora()
    {
        return new Dystrybutor(nazwa_dystrybutora());
    }

    private String nazwa_dystrybutora()
    {
        String[] nazwy_1_czlon = {"Pixel", "99 ", "Dragon ", "Lazy", "Bohemnan ", "Evil ", "SPinkaFilm", "Gregory ", "Hollywood"};
        String[] nazwy_2_czlon = {"Box", "Studios", "Lore", "Cat", "Parody", "Film", "Lion", "Art", "Produkcja"};

        String temp = nazwy_1_czlon[rand.nextInt(9)] + nazwy_2_czlon[rand.nextInt(9)];
        return temp;
    }


    public Film generator_filmu()
    {
        Film temp = new Film();
        temp.setNazwa(byt_nazwa());
        temp.setGatunek(byt_gatunek());
        temp.setOpis(generator_opisu());
        temp.setKraje_produkcji(kraj_produkcji_byt());
        temp.setZdjecie(losowanie_zdjecia());
        temp.setOcena_uzytkownikow(ocena_losowanie());
        temp.setCena(cena_losowanie());
        temp.setCzas_trwania(czas_trwania_film());
        temp.setCzas_dostepnosci(10);

        return temp;
    }

    public Serial generator_serialu()
    {
        Serial temp = new Serial();
        temp.setNazwa(byt_nazwa());
        temp.setGatunek(byt_gatunek());
        temp.setOpis(generator_opisu());
        temp.setKraje_produkcji(kraj_produkcji_byt());
        temp.setZdjecie(losowanie_zdjecia());
        temp.setOcena_uzytkownikow(ocena_losowanie());
        temp.setCena(cena_losowanie());
        temp.setCzas_trwania(czas_trwania_serialu());

        int c = rand.nextInt(5) + 1;
        for (int i = 0; i < c; i++)
        {
            Sezon tmp = generator_sezonu();
            tmp.setNazwa("Sezon " + (i + 1));
            temp.dodaj_sezon(tmp);
        }

        return temp;
    }

    private String byt_nazwa()
    {
        String[] nazwy_1_czlon = {"Blues ", "Dirty ", "Die ", "Forrest ", "Happy ", "Valiant ", "Private ", "300 ", "Peace ", "American ", "Good, Bad and ", "Lord of ", "Wind "};
        String[] nazwy_2_czlon = {"Brothers", "", "Harry", "Hard", "Gump", "Feet", "Hearts", "Ryan", "Beauty", "Ugly", "The Rings", "Maker", "Of memes"};
        String temp = nazwy_1_czlon[rand.nextInt(nazwy_1_czlon.length)] + nazwy_2_czlon[rand.nextInt(nazwy_2_czlon.length)];
        return temp;
    }

    private Gatunki byt_gatunek()
    {

        return Gatunki.values()[rand.nextInt(Gatunki.values().length)];

    }

    /**
     * laczy przygotowane moduly w opis
     *
     * @return opis
     */
    private String generator_opisu()
    {
        String[] slowa = {"Porywający ", "Niesamowity ", "Zwrot akcji ", "samotny ojciec ", "młody fan ", "Z dala od domu ", "terrorystom ", "odnalezc horom curke ", "musi ocalić ", "historia młodej kobiety ", "zmierzyć się ", "lodówka. ", "żaba kermit. ", "alkoholik. "};
        String temp = "";

        for (int i = 0; i < 4; i++)
        {
            temp += slowa[rand.nextInt(slowa.length)];
        }
        return temp;
    }

    /**
     * Wybiera kraj produkcji
     * @return  String, kraj
     */
    private String kraj_produkcji_byt()
    {
        String[] kraje = {"Polska", "Belgia", "USA", "Indie", "Wielka Brytania", "Niemcy", "Francja", "Hispzania", "Brazylia", "Rosja"};
        return kraje[rand.nextInt(kraje.length)];
    }

    /**
     * Wybór okładki
     * @return adres do zdjecia
     */
    private String losowanie_zdjecia()
    {
        switch (rand.nextInt(7))
        {
            case 0:
                return "/zdjecia/1.png";
            case 1:
                return "/zdjecia/2.jpg";
            case 2:
                return "/zdjecia/3.jpg";
            case 3:
                return "/zdjecia/4.jpg";
            case 4:
                return "/zdjecia/5.jpg";
            case 5:
                return "/zdjecia/6.jpg";
            case 6:
                return "/zdjecia/7.jpg";


        }
        return "/zdjecia/1.png";

    }

    public Sezon generator_sezonu()
    {
        int tmp = rand.nextInt(10) + 5;
        Sezon sezon = new Sezon();
        for (int i = 0; i < tmp; i++)
        {
            sezon.dodaj_odcinek(new Odcinek());
        }
        return sezon;
    }

    private int ocena_losowanie()
    {
        return rand.nextInt(100);
    }

    private double cena_losowanie()
    {
        double[] ceny = {3.99, 5.99, 9.99, 15.99};
        return ceny[rand.nextInt(ceny.length)];
    }

    private int czas_trwania_film()
    {
        return rand.nextInt(120) + 35;
    }

    private int czas_trwania_serialu()
    {
        return rand.nextInt(50) + 5;
    }


}
