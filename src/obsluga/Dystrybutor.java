package obsluga;

import byty_ogladane.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Thread.sleep;

public class Dystrybutor implements Runnable, Serializable {
    private Object umowa;
    private int ryczalt;

    private boolean flag = true;
    private volatile List<Film> Lista_Filmow = new ArrayList<>();
    private volatile List<Serial> Lista_Seriali = new ArrayList<>();
    private Generatory generatory = new Generatory();
    private String nazwa;

    public Dystrybutor(String string)
    {
        setNazwa(string);
       // System.out.println("Hey, jestem " + getNazwa() + ". Ja żyje!!!!");
        for (int i = 0; i < 2; i++)
        {
            generator_pozycji();
        }
        Thread thread = new Thread(this);
        thread.start();

    }

    public String getNazwa()
    {
        return nazwa;
    }

    public void setNazwa(String nazwa)
    {
        this.nazwa = nazwa;
    }

    public Object getUmowa()
    {
        return umowa;
    }

    public void setUmowa(Object umowa)
    {
        this.umowa = umowa;
    }

    public int getRyczalt()
    {
        return ryczalt;
    }

    public void setRyczalt(int ryczalt)
    {
        this.ryczalt = ryczalt;
    }

    public List<Film> getLista_Filmow()
    {
        return Lista_Filmow;
    }
    public List<Serial> getLista_Seriali()
    {
        return Lista_Seriali;
    }

    @Override
    public void run()
    {
        while (flag)
        {
            try
            {
                sleep(10000);
                System.out.println("Pojawiły się nowe pozycje");
                generator_pozycji();
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }

    }

    public synchronized void generator_pozycji()
    {

        Film temp;
        temp = generatory.generator_filmu();
        temp.setDystrybutor(nazwa);
        Lista_Filmow.add(temp);

        Serial temp2;
        temp2 = generatory.generator_serialu();
        temp2.setDystrybutor(nazwa);
        Lista_Seriali.add(temp2);




    }
}
