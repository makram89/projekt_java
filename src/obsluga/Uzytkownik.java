package obsluga;

public class Uzytkownik
{
    private String imie;
    private String nick;
    private int kod;
    private Object data_uro;
    private String email;
    private int nr_KartyKredytowej;
    private Abonament abonament;
    private Object wykupione_filmy_streamy;

    public String getImie()
    {
        return imie;
    }

    public void setImie(String imie)
    {
        this.imie = imie;
    }

    public String getNick()
    {
        return nick;
    }

    public void setNick(String nick)
    {
        this.nick = nick;
    }

    public int getKod()
    {
        return kod;
    }

    public void setKod(int kod)
    {
        this.kod = kod;
    }

    public Object getData_uro()
    {
        return data_uro;
    }

    public void setData_uro(Object data_uro)
    {
        this.data_uro = data_uro;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public int getNr_KartyKredytowej()
    {
        return nr_KartyKredytowej;
    }

    public void setNr_KartyKredytowej(int nr_KartyKredytowej)
    {
        this.nr_KartyKredytowej = nr_KartyKredytowej;
    }

    public Abonament getAbonament()
    {
        return abonament;
    }

    public void setAbonament(Abonament abonament)
    {
        this.abonament = abonament;
    }

    public Object getWykupione_filmy_streamy()
    {
        return wykupione_filmy_streamy;
    }

    public void setWykupione_filmy_streamy(Object wykupione_filmy_streamy)
    {
        this.wykupione_filmy_streamy = wykupione_filmy_streamy;
    }
}
