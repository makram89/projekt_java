package byty_ogladane;

public class Live extends Byt
{
    private Object data;
    private Double cena;
    private Object promocja;

    public Object getData()
    {
        return data;
    }

    public void setData(Object data)
    {
        this.data = data;
    }

    @Override
    public Double getCena()
    {
        return cena;
    }

    @Override
    public void setCena(Double cena)
    {
        this.cena = cena;
    }

    public Object getPromocja()
    {
        return promocja;
    }

    public void setPromocja(Object promocja)
    {
        this.promocja = promocja;
    }
}
