package byty_ogladane;

import java.util.ArrayList;
import java.util.List;

public class Sezon
{


    private List<Odcinek> odcinki = new ArrayList<>();
    private String nazwa;

    private Object status;

    public String getNazwa()
    {
        return nazwa;
    }

    public void setNazwa(String nazwa)
    {
        this.nazwa = nazwa;
    }



    public List<Odcinek> getOdcinki()
    {
        return odcinki;
    }




    public void dodaj_odcinek(Odcinek odcinek)
    {
        this.odcinki.add(odcinek);
    }



}
