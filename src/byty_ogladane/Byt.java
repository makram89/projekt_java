package byty_ogladane;

abstract public class Byt {


    private String zdjecie;
    private String nazwa;
    private String opis;
    private Object data_produkcji;
    private int czas_trwania;
    private String dystrybutor;
    private String kraje_produkcji;
    private int ocena_uzytkownikow;
    private Double cena;


    public String getZdjecie()
    {
        return zdjecie;
    }

    public void setZdjecie(String zdjecie)
    {
        this.zdjecie = zdjecie;
    }

    public String getNazwa()
    {
        return nazwa;
    }

    public void setNazwa(String nazwa)
    {
        this.nazwa = nazwa;
    }

    public String getOpis()
    {
        return opis;
    }

    public void setOpis(String opis)
    {
        this.opis = opis;
    }

    public Object getData_produkcji()
    {
        return data_produkcji;
    }

    public void setData_produkcji(Object data_produkcji)
    {
        this.data_produkcji = data_produkcji;
    }

    public int getCzas_trwania()
    {
        return czas_trwania;
    }

    public void setCzas_trwania(int czas_trwania)
    {
        this.czas_trwania = czas_trwania;
    }

    public String getDystrybutor()
    {
        return dystrybutor;
    }

    public void setDystrybutor(String dystrybutor)
    {
        this.dystrybutor = dystrybutor;
    }

    public String getKraje_produkcji()
    {
        return kraje_produkcji;
    }

    public void setKraje_produkcji(String kraje_produkcji)
    {
        this.kraje_produkcji = kraje_produkcji;
    }

    public int getOcena_uzytkownikow()
    {
        return ocena_uzytkownikow;
    }

    public void setOcena_uzytkownikow(int ocena_uzytkownikow)
    {
        this.ocena_uzytkownikow = ocena_uzytkownikow;
    }

    public Double getCena()
    {
        return cena;
    }

    public void setCena(Double cena)
    {
        this.cena = cena;
    }


}

