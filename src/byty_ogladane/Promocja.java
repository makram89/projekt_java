package byty_ogladane;

public class Promocja
{
    private Object wartosc_upustu;
    private Object data_rozpoczecia;
    private Object data_zakonczenia;

    public Object getWartosc_upustu()
    {
        return wartosc_upustu;
    }

    public void setWartosc_upustu(Object wartosc_upustu)
    {
        this.wartosc_upustu = wartosc_upustu;
    }

    public Object getData_rozpoczecia()
    {
        return data_rozpoczecia;
    }

    public void setData_rozpoczecia(Object data_rozpoczecia)
    {
        this.data_rozpoczecia = data_rozpoczecia;
    }

    public Object getData_zakonczenia()
    {
        return data_zakonczenia;
    }

    public void setData_zakonczenia(Object data_zakonczenia)
    {
        this.data_zakonczenia = data_zakonczenia;
    }
}
