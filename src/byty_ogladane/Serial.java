package byty_ogladane;

import java.util.ArrayList;
import java.util.List;

public class Serial extends Byt {
    private Gatunki gatunek;
    private String lista_Aktorow;
    private List<Sezon> sezony = new ArrayList<>();


    public String getGatunek()
    {
        return gatunek.toString();
    }

    public void setGatunek(Gatunki gatunek)
    {
        this.gatunek = gatunek;
    }

    public String getLista_Aktorow()
    {
        return lista_Aktorow;
    }

    public void setLista_Aktorow(String lista_Aktorow)
    {
        this.lista_Aktorow = lista_Aktorow;
    }


    public void dodaj_sezon(Sezon sezon)
    {
        sezony.add(sezon);
    }

    public List<Sezon> getSezony()
    {
        return sezony;
    }


}
