package byty_ogladane;

public class Film extends Byt {

    private Gatunki gatunek;
    private Object lista_Aktorow;
    private String link_zwiastunu;
    private int czas_dostepnosci;
    private Statusy status;
    private double promocja;

    //    Generator losowy
    public Film()
    {


    }


    public String getGatunek()
    {
        return gatunek.toString();
    }


    public void setGatunek(Gatunki gatunek)
    {
        this.gatunek = gatunek;
    }

    public Object getLista_Aktorow()
    {
        return lista_Aktorow;
    }

    public void setLista_Aktorow(Object lista_Aktorow)
    {
        this.lista_Aktorow = lista_Aktorow;
    }

    public String getLink_zwiastunu()
    {
        return link_zwiastunu;
    }

    public void setLink_zwiastunu(String link_zwiastunu)
    {
        this.link_zwiastunu = link_zwiastunu;
    }

    public int getCzas_dostepnosci()
    {
        return czas_dostepnosci;
    }

    public void setCzas_dostepnosci(int czas_dostepnosci)
    {
        this.czas_dostepnosci = czas_dostepnosci;
    }

    public Statusy getStatus()
    {
        return status;
    }

    public void setStatus(Statusy status)
    {
        this.status = status;
    }

    public double getPromocja()
    {
        return promocja;
    }

    public void setPromocja(double promocja)
    {
        this.promocja = promocja;
    }

}
