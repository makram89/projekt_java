package sample;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

import java.beans.EventHandler;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;
import java.util.ResourceBundle;

import byty_ogladane.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import obsluga.*;

import static byty_ogladane.Rodzaj_obslugi.*;

import static javafx.collections.FXCollections.observableArrayList;

public class Controller implements Initializable {

    @FXML
    Text opis_text = new Text();
    @FXML
    ListView<String> nazwy_listview = new ListView<>();
    @FXML
    ListView<String> lista_dodatkowa = new ListView<>();
    @FXML
    Label tytul_label = new Label();
    @FXML
    Label gatunek_label = new Label();
    @FXML
    Label kraj_produkcji_label = new Label();
    @FXML
    ImageView zdjecie_ImageView = new ImageView();
    @FXML
    ComboBox sezon_combo = new ComboBox();
    @FXML
    Label ile_odcinkow = new Label();
    @FXML
    Label ocena_label = new Label();
    @FXML
    Label cena_label = new Label();
    @FXML
    Label czas_trwania = new Label();
    @FXML
    Label dostepnosc_label = new Label();
    @FXML
    Label promocja_label = new Label();
    @FXML
    Label dystrybutor_label = new Label();


    @FXML
    Label produkcja_text_label = new Label();
    @FXML
    Label dystrybucja_text_label = new Label();
    @FXML
    Label gatunek_text_label = new Label();
    @FXML
    Label ocena_text_label = new Label();
    @FXML
    Label czas_text_label = new Label();
    @FXML
    Label dostepnosc_text_label = new Label();
    @FXML
    Label cena_text_label = new Label();
    @FXML
    Label promocja_text_label = new Label();
    @FXML
    Button usun_button = new Button();

    @FXML
    Label nazwa_obiektu = new Label();


    private Rodzaj_obslugi rodzaj_obslugi = nic_enum;

    private boolean usunac_flaga = false;
    private Generatory generatory = new Generatory();

    private List<Film> films = new ArrayList<>();
    private List<Serial> serials = new ArrayList<>();
    private List<Dystrybutor> dystrybutors = new ArrayList<>();

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {


        //generator_filmow();

        generator_dystrybutorow(5);


    }

    /**
     * Pozwala ustalić widzialoność wielu obiektów na raz
     * @param flaga
     */
    private void niewidzoialnosc(boolean flaga)
    {
        produkcja_text_label.setVisible(flaga);
        cena_text_label.setVisible(flaga);
        dostepnosc_text_label.setVisible(flaga);
        czas_text_label.setVisible(flaga);
        ocena_text_label.setVisible(flaga);
        gatunek_text_label.setVisible(flaga);
        dystrybucja_text_label.setVisible(flaga);
        produkcja_text_label.setVisible(flaga);
        promocja_text_label.setVisible(flaga);
        usun_button.setVisible(!flaga);
        lista_dodatkowa.setVisible(!flaga);
        ile_odcinkow.setVisible(flaga);
        zdjecie_ImageView.setVisible(flaga);
        opis_text.setVisible(flaga);
        promocja_label.setVisible(flaga);
        cena_label.setVisible(flaga);
        dostepnosc_label.setVisible(flaga);
        czas_trwania.setVisible(flaga);
        ocena_label.setVisible(flaga);
        gatunek_label.setVisible(flaga);
        dystrybutor_label.setVisible(flaga);
        promocja_label.setVisible(flaga);
        kraj_produkcji_label.setVisible(flaga);
        sezon_combo.setVisible(false);
    }

    public void generator_dystrybutorow(int c)
    {
        for (int i = 0; i < c; i++)
        {
            Dystrybutor temp;
            temp = generatory.generator_dystrybutora();
            dystrybutors.add(temp);
        }
    }


    /**
     * Funkcja służy do wyświetlania listy filmów
     * wyzwala ja nacisciecie przycisku
     */
    @FXML
    public synchronized void wyswietl_filmy()
    {
        niewidzoialnosc(true);

        ObservableList dane = observableArrayList();
        sezon_combo.setVisible(false);

        rodzaj_obslugi = filmy_enum;

        films = new ArrayList<>();

        int tmp = dystrybutors.size();
        List<Film> temp = new ArrayList<>();
        for (int i = 0; i < tmp; i++)
        {
            temp = dystrybutors.get(i).getLista_Filmow();
            int tmp2 = temp.size();
            for (int j = 0; j < tmp2; j++)
            {
                dane.add(temp.get(j).getNazwa());
                films.add(temp.get(j));
            }
        }

        nazwy_listview.setItems(dane);

    }

    @FXML
    public void wyswietl_film(int index)
    {

        Film dane = films.get(index);
        tytul_label.setText(dane.getNazwa());
        opis_text.setText(dane.getOpis());
        gatunek_label.setText(dane.getGatunek());
        kraj_produkcji_label.setText(dane.getKraje_produkcji());
        zdjecie_ImageView.setImage(new Image(dane.getZdjecie()));
        ocena_label.setText(String.valueOf(dane.getOcena_uzytkownikow()));
        cena_label.setText(dane.getCena().toString() + " zł");
        czas_trwania.setText(String.valueOf(dane.getCzas_trwania()) + " min");
        dostepnosc_label.setText(String.valueOf(dane.getCzas_dostepnosci()) + " dni");
        promocja_label.setText("0" + " %");
        dystrybutor_label.setText(dane.getDystrybutor());
    }

    @FXML
    public void dodaj()
    {
        generator_dystrybutorow(1);
    }

    @FXML
    public void obsluga_pozcycji()
    {
//        System.out.println("Wciscieta pozycja " + nazwy_listview.getSelectionModel().getSelectedItem());
//        System.out.println(" Indeks " + nazwy_listview.getSelectionModel().getSelectedIndex());
        int index = nazwy_listview.getSelectionModel().getSelectedIndex();
        switch (rodzaj_obslugi)
        {
            case filmy_enum:
                wyswietl_film(index);
                break;
            case nic_enum:

                break;
            case seriale_enum:
                wyswietl_serial(index);
                break;
            case usuwanie:
                tytul_label.setText(dystrybutors.get(index).getNazwa());
                wyswietlenie_listy(dystrybutors.get(index));
                break;

        }
    }

    /**
     * Funkcja służy do wyświetlania listy seriali
     *
     */
    @FXML
    public synchronized void wyswietl_seriale()
    {
        niewidzoialnosc(true);
        ObservableList dane = observableArrayList();
        sezon_combo.setVisible(true);
        ile_odcinkow.setVisible(true);
        rodzaj_obslugi = seriale_enum;

        int tmp = dystrybutors.size();
        List<Serial> temp = new ArrayList<>();
        for (int i = 0; i < tmp; i++)
        {
            temp = dystrybutors.get(i).getLista_Seriali();
            int tmp2 = temp.size();
            for (int j = 0; j < tmp2; j++)
            {
                dane.add(temp.get(j).getNazwa());
                serials.add(temp.get(j));
            }

            nazwy_listview.setItems(dane);
        }
    }

    @FXML
    public void wyswietl_serial(int index)
    {

        Serial dane = serials.get(index);
        tytul_label.setText(dane.getNazwa());
        opis_text.setText(dane.getOpis());
        gatunek_label.setText(dane.getGatunek());
        kraj_produkcji_label.setText(dane.getKraje_produkcji());
        zdjecie_ImageView.setImage(new Image(dane.getZdjecie()));
        ocena_label.setText(String.valueOf(dane.getOcena_uzytkownikow()));
        cena_label.setText(dane.getCena().toString() + " zł");
        czas_trwania.setText(String.valueOf(dane.getCzas_trwania()) + " min");
        // dostepnosc_label.setText(String.valueOf(dane.getCzas_dostepnosci()) + " dni");
        promocja_label.setText("0" + " %");
        dystrybutor_label.setText(dane.getDystrybutor());
        ObservableList<String> sezony = observableArrayList();
        for (Sezon sezon : dane.getSezony())
        {
            sezony.add(sezon.getNazwa());

        }
        sezon_combo.setItems(sezony);

    }

    /**
     * Obsługa okna niedokończonego programu
     */
    @FXML
    public void feature()
    {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Feature.fxml"));
        try
        {
            Parent root = loader.load();
            Stage stage2 = new Stage();
            stage2.setTitle("Problem!");
            stage2.setScene(new Scene(root));
            stage2.show();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Zakladaka dystrybutorzy
     */
    public void usuwanie()
    {
        int index = nazwy_listview.getSelectionModel().getSelectedIndex();

        if (index >= 0)
        {
            dystrybutors.remove(index);
            wyswietlenie_dystrybutorow();
        }

    }


    @FXML
    public void wyswietlenie_dystrybutorow()
    {
        rodzaj_obslugi = usuwanie;
        niewidzoialnosc(false);
        ObservableList dane = observableArrayList();
        for (int i = 0; i < dystrybutors.size(); i++)
        {
            dane.add(dystrybutors.get(i).getNazwa());


        }
        nazwy_listview.setItems(dane);

    }

    /**
     * Metoda do wypełniania listy filmow i seriali stworzonych przez dystrybutora
     * @param dystrybutor
     */
    @FXML
    public void wyswietlenie_listy(Dystrybutor dystrybutor)
    {
        ObservableList dane = observableArrayList();
        List<Film> tablica = new ArrayList();
        tablica = dystrybutor.getLista_Filmow();
        for (Film film : tablica)
        {
            dane.add(film.getNazwa());
        }
        List<Serial> tablica2 = new ArrayList();
        tablica2 = dystrybutor.getLista_Seriali();
        for (Serial serial : tablica2)
        {
            dane.add(serial.getNazwa());

        }
        lista_dodatkowa.setItems(dane);
    }


}
